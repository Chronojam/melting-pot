package api

import (
	"fmt"
	"log"
	"net/http"
)

func New() *api {
	return &api{}
}

type api struct {
	mux *http.ServeMux
	port int
	debug bool
}

func (a *api) SetDebug(val bool) *api {
	a.debug = val
	return a
}

func (a *api) SetMux(mux *http.ServeMux) *api {
	a.mux = mux
	return a
}

func (a *api) SetPort(port int) *api {
	a.port = port
	return a
}

func (a *api) ConstructRoute(route string, handler http.Handler) *api {
	if a.debug {
		log.Printf("registering route: %v", route)
	}
	a.mux.Handle(route, handler)
	return a
}

func (a *api) Serve() *api{
	if a.debug {
		log.Printf("service starting on 0.0.0.0:%d", a.port)
	}
	http.ListenAndServe(
		fmt.Sprintf(":%d", a.port),
		a.mux,
	)
	return a
}