package auth

import (
	"log"
	"context"
	"io/ioutil"
	"encoding/json"
	"net/http"

	"database/sql"
)

type AuthMethod interface {
	http.Handler

	SetDatabase(db *sql.DB) AuthMethod
}

func NewUsernamePassword() *usernamePassword {
	return &usernamePassword{}
}

type usernamePassword struct {
	db *sql.DB
}

func (u *usernamePassword) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("%v", err)
		return
	}
	var pbRequest UsernamePasswordRequest
	err = json.Unmarshal(b, &pbRequest)
	if err != nil {
		log.Printf("%v", err)
		return
	}
	_, err = u.GrpcAuth(context.Background(), &pbRequest)
	if err != nil {
		log.Printf("%v", err)
		return
	}
}
func (u *usernamePassword) GrpcAuth(ctx context.Context, req *UsernamePasswordRequest) (*UsernamePasswordResponse, error) {
	return nil, nil
}

func (u *usernamePassword) SetDatabase(db *sql.DB) AuthMethod {
	u.db = db
	return u
}