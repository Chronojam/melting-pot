package main

import (
	"net/http"
	"log"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/chronojam/melting-pot/api/pkg/api"
	"gitlab.com/chronojam/melting-pot/api/pkg/auth"
)

func main() {
	db, dbErr := sql.Open("sqlite3", "./foo.db")
	if err := db.Ping(); err != nil {
		log.Fatalf("%v: %v", dbErr, err)
	}
	defer db.Close()
	api.New().
		SetDebug(true).
		SetMux(&http.ServeMux{}).
		SetPort(10111).
		ConstructRoute("/login/unpw", auth.NewUsernamePassword().SetDatabase(db)).
		ConstructRoute("/login/oauth2", auth.NewUsernamePassword().SetDatabase(db)).
		Serve()
}