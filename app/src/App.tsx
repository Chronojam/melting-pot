import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

import Login from "./containers/Login";

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/">
          <Route path="/" element={<Login/>} />
        </Route>
      </Routes>
    </Router>
  )
}